using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoroutineManager : Singleton<CoroutineManager>
{
    protected CoroutineManager(){}
}

public class Job
{
    public event System.Action<bool> OnJobComplete;

    private bool _running;
    public bool Running{ get { return _running; } }

    private bool _paused;
    public bool Paused{ get { return _paused; } }

    private IEnumerator _coroutine;
    private bool _jobWasKilled;
    private Stack<Job> _childJobStack;

    #region constructors
    public Job(IEnumerator coroutine) : this(coroutine,true){}
    public Job(IEnumerator coroutine,bool shouldStart)
    {
        _coroutine = coroutine;
        if (shouldStart)
            Start();
    }

    #endregion

    #region static job makers

    public static Job Make(IEnumerator coroutine)
    {
        return new Job(coroutine);
    }

    public static Job Make(IEnumerator coroutine,bool shouldStart)
    {
        return new Job(coroutine, shouldStart);
    }
    #endregion

    #region public API

    public Job CreateAndAddChildJob(IEnumerator coroutine)
    {
        var j = new Job(coroutine, false);
        AddChildJob(j);
        return j;
    }

    public void AddChildJob(Job childJob)
    {
        if (_childJobStack == null)
            _childJobStack = new Stack<Job>();

        _childJobStack.Push(childJob);
    }

    public void RemoveChildJob(Job childJob)
    {
        if (_childJobStack.Contains(childJob))
        {
            var childStack = new Stack<Job>(_childJobStack.Count - 1);
            var allCurrentChildren = _childJobStack.ToArray();
            System.Array.Reverse(allCurrentChildren);

            for (int i = 0; i < allCurrentChildren.Length; i++)
            {
                var j = allCurrentChildren[i];
                if (j != childJob)
                    childStack.Push(j);
            }

            _childJobStack = childStack;
        }
    }

    public void Start()
    {
        _running = true;
        CoroutineManager.Instance.StartCoroutine(DoWork());
    }

    public IEnumerator StartAsCoroutine()
    {
        _running = true;
        yield return CoroutineManager.Instance.StartCoroutine(DoWork());
    }

    public void Pause()
    {
        _paused = true;
    }

    public void Unpause()
    {
        _paused = false;
    }

    public void Kill()
    {
        _jobWasKilled = true;
        _running = false;
        _paused = false;
    }

    public void KillWithDelay(float delayInSeconds)
    {
        int delay = (int)(delayInSeconds * 1000);
        new System.Threading.Timer(obj =>
        {
            lock(this)
            {
                Kill();
            }
        }, null, delay, System.Threading.Timeout.Infinite);
    }
    #endregion

    private IEnumerator DoWork()
    {
        //null out the first run in case we start paused
        yield return null;

        while (_running)
        {
            if (_paused)
            {
                yield return null;
            }
            else
            {
                //run the next iteration and stop when we are done
                if (_coroutine.MoveNext())
                {
                    yield return _coroutine.Current;
                }
                else
                {

                    //run our child jobs if we have any
                    if (_childJobStack != null)
                        yield return CoroutineManager.Instance.StartCoroutine(RunChildJobs());
                    _running = false;
                }
            }
        }

        //fire off complete even
        if (OnJobComplete != null)
            OnJobComplete(_jobWasKilled);
    }

    private IEnumerator RunChildJobs()
    {
        if (_childJobStack != null && _childJobStack.Count > 0)
        {
            do
            {
                Job childJob = _childJobStack.Pop();
                yield return CoroutineManager.Instance.StartCoroutine(childJob.StartAsCoroutine());
            }
            while(_childJobStack.Count > 0);
        }
    }
}
