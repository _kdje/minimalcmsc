﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Test : MonoBehaviour {

    public Button urlButton;
    public Renderer renderer;
    public Image image;
    readonly string url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYXVTbxmHQuOogxDeNx7U6z7neHX4kOQPZEBLG5nzQZoMMhkFN";


    private void Awake()
    {
        urlButton.onClick.AddListener(urlButtonClicked);
    }

    IEnumerator Start()
    {
        // Start a download of the given URL
        using (WWW www = new WWW(url))
        {
            // Wait for download to complete
            yield return www;

            // assign texture
            image.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2());
            //renderer.material.mainTexture = www.texture;
        }
    }
    void urlButtonClicked()
    {
        Debug.Log("urlButtonClicked()");
        Application.OpenURL("https://developer.vuforia.com/targetmanager/licenseManager/licenseListing");
    }
}
